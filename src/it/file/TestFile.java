package it.file;

public class TestFile {
    public static void main(String[] args) {
        FileSorgente myFile = new FileSorgente("prova.txt", MyFile.TIPO_DI_FILE.TXT, "ciao");
        myFile.aggiungiTesto();
        System.out.println(myFile.getContenuto());
        try {
            myFile.aggiungiTesto(2);
            System.out.println(myFile.getContenuto());
        }
        catch (NullPointerException e) {
            System.out.println("Non è possibile aggiungere qualcosa al contenuto del sito!");
        }
        catch (IndexOutOfBoundsException e) {
            System.out.println("La posizione scelta non è valida!");
        }
    }
}
