package it.file;

public class MyFile {

    public enum TIPO_DI_FILE {
        JAVA,
        C,
        C_SHARP,
        PYTHON,
        TXT;
    }
    private String nome;
    private TIPO_DI_FILE tipoDiFile;

    public MyFile(String nome, TIPO_DI_FILE tipoDiFile) {
        this.nome = nome;
        this.tipoDiFile = tipoDiFile;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public TIPO_DI_FILE getTipoDiFile() {
        return this.tipoDiFile;
    }

    public void setTipoDiFile(TIPO_DI_FILE tipoDiFile) {
        this.tipoDiFile = tipoDiFile;
    }
}
