package it.file;

import java.util.Scanner;

public class FileSorgente extends MyFile {


    private String contenuto = "";

    public FileSorgente(String nome, MyFile.TIPO_DI_FILE tipoDiFile, String contenuto) {
        super(nome, tipoDiFile);
        this.contenuto = contenuto;
    }

    public void aggiungiTesto() {
        Scanner in = new Scanner(System.in);
        System.out.println("Inserire ciò che si vuole: ");
        this.contenuto = this.contenuto.concat(in.next());
    }

    public void aggiungiTesto(int posizione) {
        if (posizione > this.contenuto.length()) {
            throw new IndexOutOfBoundsException();
        }
        if (this.contenuto.isEmpty())
            throw new NullPointerException();
        String nuovoContenuto= "";
        nuovoContenuto = nuovoContenuto.concat(this.contenuto.substring(0, posizione));
        Scanner in = new Scanner(System.in);
        System.out.println("Inserire ciò che si vuole: ");
        nuovoContenuto = nuovoContenuto.concat(in.nextLine());
        nuovoContenuto = nuovoContenuto.concat(this.contenuto.substring(posizione));
        this.contenuto = nuovoContenuto;
    }

    public String getContenuto() {
        return this.contenuto;
    }

    public void setContenuto(String contenuto) {
        this.contenuto = contenuto;
    }
}
